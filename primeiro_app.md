# [Angular 2 - Getting Started](README.md) #
  
  
## Assuntos

1. Components  
- @Component  
  
2. Services  
- @Injectable    
  
3. Http    
4. Models
  
  
## Primeiro APP

### Iniciando um APP com Sidebar

- Abra o CMD e v� at� uma pasta para iniciar o projeto. Rode o comando abaixo para iniciar sua aplica��o em ionic2.  
  
```
ionic start --v2 nomeAplicacao sidemenu
```

Este passo pode demorar.

  
### Analisando a estrutura das pastas
  
Acesse a pasta *nomeAplicacao* que � o nome que voc� deu a sua aplica��o e analise as pastas abaixo:  
  
*config.xml*  
Este � o arquivo de configura��o do cordova. Quando o ionic transformar a aplica��o em android, ios, etc ele vai chamar o Cordova e o cordova vai ler esse arquivo de configura��o.  

*tsconfig.json*  
Arquivo de configura��o do typescript. Ele ensina ao typescript qual pasta deve ser compilada para javascript, qual n�o deve e qual ser� a vers�o do javascript para qual o c�digo ser� transpilado.  
  
*Pasta src*  
Esta � a pasta do arquivo fonte da aplica��o. Os arquivos typescript, scss, html, etc ficam aqui.  
  
*Pasta build*  
Esta pasta � criada quando o ionic compila a aplica��o.  
  
*Pasta node_modules*  
Esta pasta armazena todas as bibliotecas utilizadas pelo angular, ionic. � como a pasta "vendor" do composer.  
  
  
  
### Visualizando a aplica��o no navegador
  
Dentro da pasta do projeto rode o comando abaixo:  
  
```
ionic serve
```
O browser dever� abrir a aplica��o no endere�o: http://localhost:8100



### Adicionando componentes  
  
Todo componente deve ser conhecido pelo m�dulo principal ( root module ). O m�dulo principal fica em src/app/app.module.ts.
Quando incluir um componente ( Uma nova p�gina ) importe o novo componente para dentro do bloco "declarations" e do bloco "entryComponents" do m�dulo principal.  

  
O primeiro componente a ser criado dever� ser uma tela de login que aparece no menu sidebar.  
  
*Tela de Login*  
  
## 1. Crie a pasta "login" dentro de src/pages e dentro de login crie a seguinte estrutura:
```
src
 |_pages/ 
   |_login/
      |__login.ts
      |__login.html
      |__login.scss
```

## 2. O arquivo login.ts � o componente ( p�gina ) que estamos criando. O conte�do necess�rio para esse arquivo �:

```
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'agil-login',
  templateUrl: 'login.html'
})
export class Login {

  constructor(public navCtrl: NavController) {
    
  }

}

```

O annotation @Component indica que essa classe � um componente visual ( tela ). O atributo "selector" indica que esse componente pode ser utilizado pela tag <agil-login></agil-login>. O templateUrl indica o caminho relativo para o template deste componente.  
  
   
  
  
  
  
## 3. O template login.html pode ser escrito utilizando os componentes fornecidos pelo Ionic. Para a lista de componentes do ionic, acesse o link:
  
Link: https://ionicframework.com/docs/v2/components/#overview  
  
index.html
```
<ion-header>
  <ion-navbar>
    <button ion-button menuToggle>
      <ion-icon name="menu"></ion-icon>
    </button>
    <ion-title>Login</ion-title>
  </ion-navbar>
</ion-header>

<ion-content padding>
    <ion-card class="login">
        <img src="http://basesoft.com.br/agil/imagens/login/agil_logo.png" style="width: 99px; height: 85px;">
        <ion-card-content>    
            <ion-list>
                <ion-item>
                    <ion-label stacked>E-mail</ion-label>
                    <ion-input type="text"></ion-input>
                </ion-item>
                <ion-item>
                    <ion-label stacked>Senha</ion-label>
                    <ion-input type="password"></ion-input>
                </ion-item>
                <ion-item>
                    <button ion-button>Entrar</button>     
                </ion-item>
            </ion-list>
        </ion-card-content>
    </ion-card>    
</ion-content>
```
  
  
  
  
  
  
## 4. ADICIONANDO O NOVO COMPONENTE  
  
  
  
### 4.1 Ao m�dulo principal  
  
app.module.ts
```
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { Login } from '../pages/login/login';

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    Login
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    Login
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}

```



## 5. Estilo do formulario:

login.scss
```
.login {
    text-align: center; 

    img {
        width: 99px;
        height: 85px;
        margin: auto;
    }
}
```
  
  
## 6. 