import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BuscaImoveisService } from './../../app/services/buscaimoveis.service';
import { Imovel } from './../../app/models/imovel';


@Component({
  selector: 'page-page2',
  templateUrl: 'page2.html'
})
export class Page2 {
  public imoveis : Imovel[];

  constructor(public navCtrl: NavController, public buscaImoveis : BuscaImoveisService) {
    this.imoveis = [];
    this.buscarImoveis();
  }

  buscarImoveis(){    
    this.buscaImoveis.getImoveis('Jacarepagua').subscribe(
        imoveis => this.imoveis = imoveis
    );
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(Page2, {
      item: item
    });
  }
}
