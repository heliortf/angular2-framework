import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BuscaImoveisService } from './../../app/services/buscaimoveis.service';
import { Imovel } from './../../app/models/imovel';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {

  public imoveis : Imovel[];

  constructor(public navCtrl: NavController, public buscaImoveis : BuscaImoveisService) {
    this.imoveis = [];
    this.buscarImoveis();
  }

  buscarImoveis(){    
    this.buscaImoveis.getImoveis('Barra da Tijuca').subscribe(
        imoveis => this.imoveis = imoveis
    );
  }
}
