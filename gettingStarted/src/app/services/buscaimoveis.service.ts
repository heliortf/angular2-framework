import { Injectable } from '@angular/core';
import { Http, Response, Jsonp, URLSearchParams } from '@angular/http';
import { Imovel } from './../models/imovel';
import { Observable } from 'rxjs/Observable';
import {  } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class BuscaImoveisService {
    
    protected url : string;

    constructor(private http : Http, private jsonp: Jsonp){
        this.url = "http://crmead.basesoft.com.br/api/angular/imoveis"
    }

    public getImoveis(bairro) : Observable<Imovel[]> {

        let params = new URLSearchParams();
        params.set('Bairros', bairro);
        params.set('qtd_por_pagina', '20');
        params.set('callback', 'JSONP_CALLBACK');

        return this.jsonp.get(this.url, { search: params })
                    .map(this.mapearImoveis) 
                    .catch(this.handleError);
    }

    private mapearImoveis(res : Response){
      let body = res.json();
      return body || [];
    }

    private handleError(error : Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
          const body = error.json() || '';
          const err = body.error || JSON.stringify(body);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
          errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}