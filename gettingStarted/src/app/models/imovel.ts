
export class Imovel {
    entidadeId : Number;
    titulo : String;
    descricao : String;
    imagem : String;
    valor : Number;
    municipio : String;
    bairro : String;
}