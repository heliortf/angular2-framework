# [Angular 2 - Getting Started](README.md) #


### Seu primeiro deploy
  
Para que seja possível fazer deploy para uma plataforma é necessário primeiro incluí-la no projeto. Como nosso primeiro deploy é para android, então vamos incluí-lo no projeto. Para isso, dentro da pasta do projeto execute:

```
ionic platform add android
```

Agora podemos fazer o build do nosso app para Android. 


### Adicionando outras plataformas
  
Se tivessemos o SDK ios instalado poderiamos rodar o comando:

```
ionic platform add ios
```

